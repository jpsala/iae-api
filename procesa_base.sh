#!/bin/sh
now=$(date -d "today" +"%Y%m%d-%H%M")
file="$1"
#[ -f "$file" ] || (echo "$FILE NOT FOUND" && exit 1)
[ -f "$file" ] || { echo "$file NOT FOUND" ; exit 1 ;}

echo dump local
mysqldump -a -u root -plani0363 iae-nuevo > iae-nuevo.local.dump

echo bzip2 local
bzip2 iae-nuevo.local.dump
mv iae-nuevo.local.dump.bz2 iae-nuevo.local.dump.$now.bz2

echo drop
mysqladmin drop iae-nuevo -u root -plani0363 -f

echo create
mysqladmin create --default-character-set=utf8 iae-nuevo -u root --password=lani0363 -f

echo bunzip2

#[ -f "iae-nuevo.dump" ] && { rm iae-nuevo.dump  ;}
#bunzip2 -dc $file > iae-nuevo.dump
echo import
mysql iae-nuevo -u root --password=lani0363<$file
