<?php
class ApiAppController extends Controller
{
  private $body, $resp, $user, $get, $post, $headers, $auth_token, $minutesForTimeout = 30;

  public function actionError()
  {
    if ($error = Yii::app()->errorHandler->error)
      $this->render('error', $error);
        // echo 'hola';
  }
  public function missingAction($action)
  {
    $this->action = $action;
    $this->chkRequestForCors();
    // var_dump('hola');
    // die;
    $this->setRequestData();
    $publicActions = [
      'auth',
      'logout',
      'activate',
      'register'
    ];
    if (!in_array($action, $publicActions)) {
      $this->chkSession();
    }
    $this->exec($action);
    return true;
  }

  private function chkRequestForCors()
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
    header("Access-Control-Allow-Headers: authorization, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
      exit(0);
    }
  }

  private function setRequestData()
  {
    $this->body = json_decode(file_get_contents('php://input'));
    $this->resp = new stdClass();
    $this->resp->status = 200;
    $this->resp->minutesForTimeout = $this->minutesForTimeout;
    $this->get = $_GET;
    $this->post = $_POST;
    $this->headers = apache_request_headers();
    $auth = isset($this->headers['authorization']) ? $this->headers['authorization'] : false;
    $auth = $auth ? $auth : (isset($this->headers['Authorization']) ? $this->headers['Authorization'] : 'no authorization in headers');
    $this->auth_token = $auth;
  }

  protected function chkSession()
  {
    if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
      exit(0);
    }

    $jwt = '';
    $this->resp->action = $this->action;
    $logging = $this->resp->action == 'login';
    $tokenIsValid = ($this->auth_token and (strlen($this->auth_token) > 20));
    $loggingConToken = ($this->action === 'login' and !isset($this->body->email));
    $loggingConCredenciales = ($this->action === 'login' and isset($this->body->email));
    $saveCredentials = (isset($this->body->saveCredentials) and ($this->body->saveCredentials === 'true'));
    try {
      if ($loggingConCredenciales) { //* de acá hace un exit
        $email = isset($this->body->email) ? $this->body->email : null;
        $password = isset($this->body->password) ? $this->body->password : null;
        $saveCredentials = (boolean)$this->body->saveCredentials;
        $where = "where login = '$email' and (password = MD5('$password') or password = '$password')";
        $socio = Helpers::qry("
          SELECT id FROM user $where
        ");
        if ($socio) {
          $decode = new stdClass();
          $decode->user = $socio['id'];
          if ($saveCredentials) {
            $decode->date = time() + 3600 * 24 * 7;
          } else {
            $decode->date = time() + ($this->minutesForTimeout * 60);
          }
          $this->resp->date = date('d/m/Y', $decode->date);
          $this->resp->jwtDecodedDespues = $decode;
          $jwt = Yii::app()->JWT->encode($decode);
          $this->resp->access_token = $jwt;
          $this->resp->status = 200;
          $this->resp->user = $socio['id'];
          $this->resp->hijos = getHijos($socio['id']);
        } else {
          //! no encontró el usuario que quiere ingresar
          $this->resp->status = 401;
          $this->resp->statusMsj = 'Error en token de autenticación';
        }
        exit(json_encode($this->resp));
      }

      if ($tokenIsValid) {
        $decode = Yii::app()->JWT->decode($this->auth_token);
        $diff = $decode->date - time();
        $minutesRestantes = round($diff / 60, 2);
        $this->resp->minutosRestantes = $minutesRestantes;
        $this->resp->tokenValido = $decode;
        $this->resp->loggingConToken = $loggingConToken;
        // if($minutesTranscurridos > $this->minutesForTimeout){
        if (time() > $decode->date) {
          $this->resp->status = 402;
          $this->resp->error = 'Timeout';
        } else {
          if ($minutesRestantes < 31) {
            $decode->date = time() + ($this->minutesForTimeout * 60);
          }
          $decode->user = $decode->user;
          $this->user = $decode->user;
          $jwt = Yii::app()->JWT->encode($decode);
          $this->resp->access_token = $jwt;
          $this->resp->user = $decode->user;
          $this->resp->hijos = getHijos($decode->user);
          $this->resp->date = date('d/m/Y', $decode->date);
          $this->resp->jwtDecodedDespues = $decode;
          $this->resp->status = 200;
          if ($loggingConToken) {
            //* no ejecuto ninguna acción(return), vuelvo al cliente
            exit(json_encode($this->resp));
          }
          return true;
        }
      } else {
        $this->resp = new stdClass();
        $this->resp->access_token = $this->auth_token;
        $this->resp->status = 403;
        $this->resp->statusMsj = 'No hay ningún token';
        exit(json_encode($this->resp));
      }
    } catch (Exception $e) {
      // $this->resp = new stdClass();
      $this->resp->access_token = $this->auth_token;
      $this->resp->status = 401;
      $this->resp->statusMsj = 'Error en token de autenticación' . json_encode($e);
      exit(json_encode($this->resp));
    }
  }

  private function exec($action)
  {
    $actionFile = getcwd() . '/protected/controllers/apiApp/' . $action . '.php';
    if (!file_exists($actionFile)) {
      throw new Exception('no existe ' . $actionFile);
    }
    // $controller = $this;
    require($actionFile);
  }

}

function getHijos($socio_id)
{

};
