<?php
function graba($doc, $puedeAplicar = true) {
  $doc->fecha = Helpers::fechaParaGrabar($doc->fecha);
  $docTipo = (object) Helpers::qry("select * from doc_tipo where id = $doc->doc_tipo_id");
  $return = (object) ['resp'=>'', 'lastId' => ''];
  $doc->empleado_id = (isset($doc->empleado_id) && ($doc->empleado_id !== '')) ? $doc->empleado_id : 'NULL';
  $doc->detalle = (isset($doc->detalle) && ($doc->detalle !== '')) ? $doc->detalle : '';
  $doc->socio_id = (isset($doc->socio_id) && ($doc->socio_id !== '')) ? $doc->socio_id : 'NULL';
  $doc->pago = (isset($doc->pago) && ($doc->pago !== '')) ? $doc->pago : 0;
  try{
    if($doc->id === -1) { // documento nuevo
      // Alguna validación
      if($docTipo->cancela and $puedeAplicar) { // op o recibo
        $ok = true;
        $error = "";
        // vd2($doc->valores);
        if(isset($doc->cancelados)){
          $totalCancelado = 0;
          // vd2($doc->cancelados);
          foreach ($doc->cancelados as $valor) {
            $totalCancelado += $valor->saldo;
          }
          $total = 0;
          foreach ($doc->valores as $valor) {
            $total += $valor->importe;
          }
          if($doc->total !== $total and $total !== $totalCancelado) {
            $ok = false;
            $error = "En un pago la suma de los valores tiene que ser igual al total de pagos y al total de valores (Total doc:". $doc->total . " valores:" . $total . " Pagos:" . $totalCancelado . ")";
          }
        } else {
          $ok = false;
          $error = "No hay valores";
        }
        if(!$ok) {
          throw new Exception($error);
        }
      }
      // Fin validación

      // Si es un remito no grabo totales
      if($docTipo->tipo === 'stock'){
        $doc->total = 0;
        $doc->saldo = 0;
      }
      // Fin Si es un remito no grabo totales

      // grabo el doc
      $doc->saldo = $doc->total;
      $qry = "
      insert into 
        doc(doc_tipo_id, fecha, socio_id, user_id, total, saldo, numero, empleado_id, detalle)
        values($doc->doc_tipo_id, '$doc->fecha', $doc->socio_id, $doc->user_id, $doc->total, $doc->saldo, $doc->numero, $doc->empleado_id, '$doc->detalle' );
      ";
      $resp = Helpers::qryExec($qry);
      if($resp === 0) {
        // ve2($doc);
        throw new Exception("no se pudo insertar el registro en la base");
      }
      // Fin grabo el doc

      // Actualizo el numero de comprobante
      Helpers::qryExec("update doc_tipo set ultimo_numero = ultimo_numero+1 where id = $docTipo->id");
      $lastId = Helpers::qryScalar('SELECT LAST_INSERT_ID();');
      if(!$resp or !$lastId) {
        throw new Exception("algo falló y no se pudo grabar el documento" );
      }
      $doc->id = $lastId;
      // Fin Actualizo el numero de comprobante

      // Grabo el detalle
      if(isset($doc->det) and count($doc->det) > 0){
        grabaDet($doc, $lastId, $docTipo);
      }
      // Fin Grabo el detalle

      if($docTipo->cancela){ // Es un recibo o una orden de pago
        // hay documentos (factura,etc) que ejecutan generaPago() y aplican ahí
        // así que no aplican acá
        if($puedeAplicar){
          aplica($doc);
        }
      }else{ // No es ni recibo ni orden de pago
        // si tiene valores genero un pago y tambien grabo los valores
        if($docTipo->genera_pago and isset($doc->socio_id) and $doc->socio_id != 'NULL' and isset($doc->valores) and ($doc->saldo > 0)){
          generaPago($doc);
        }
      }
      if($docTipo->valores and !$docTipo->genera_pago) {
        grabaValores($doc, $lastId, $docTipo);
      }

    return (object) ['lastId' => $lastId, 'qryInsert' => $resp];
    }
  } catch(Exception $e) {
    // hubo un error!!!
    // vd2($e);
    // ve2($e);
    return (object) ['data' => $e, 'error' => true];
  }
  // return $return;
}

function grabaDet($doc, $lastId, $docTipo){
  foreach ($doc->det as $det) {
    if($docTipo->tipo === 'stock'){
      $det->precio_compra = 0;
      $det->precio_venta = 0;
      $det->total = 0;
    }
  
    $det->deposito_id = (isset($det->deposito_id) && ($det->deposito_id !== 'null')) ? $det->deposito_id : 'NULL';
    $det->mano_de_obra = ((float) $det->mano_de_obra ) <= 0 ? 0 : $det->mano_de_obra;
    $qry = "
      insert into 
        doc_det(doc_id, producto_id, deposito_id, cantidad, precio_compra, precio_venta, mano_de_obra, total)
        values($lastId, $det->producto_id, $det->deposito_id, $det->cantidad, $det->precio_compra, $det->precio_venta, $det->mano_de_obra,  $det->total)
    ";
    $resp = Helpers::qryExec($qry);              
    $lastDetId = Helpers::qryScalar('SELECT LAST_INSERT_ID();');
    $det->id = $lastDetId;
    if($det->deposito_id){
      $cantidad = (float) $det->cantidad * $docTipo->signo_stock;
      $idStock = Helpers::qryScalar("
        SELECT s.id FROM stock s WHERE s.producto_id = $det->producto_id AND s.deposito_id = $det->deposito_id
      ");
      // $cantidad = (float) $cantidad * ($docTipo->tipo === 'compra' ? 1 : -1);
      if($idStock) {
        Helpers::qryExec("
          update stock set cantidad = cantidad + $cantidad where id = $idStock
        ");
      } else {
        Helpers::qryExec("
          insert into stock(producto_id, deposito_id, cantidad) values( $det->producto_id, $det->deposito_id, $cantidad)
        ");
      }
    }
  }
}

function grabaValores($doc, $lastId, $docTipo){
  foreach ($doc->valores as $det) {
    $det->numero = (float) $det->numero;
    $det->gasto = (isset($det->gasto) && ($det->gasto !== '')) ? $det->gasto : 0;
    $det->recargo = (float) $det->gasto;
    $qry = "
      insert into 
        doc_valor(doc_id, doc_valor_tipo_id, importe, numero, gasto)
          values($lastId, $det->doc_valor_tipo_id, $det->importe, $det->numero, $det->gasto)
    ";
    $resp = Helpers::qryExec($qry);              

  }
}

function generaPago($docOrigen){
  $total = 0;
  if($docOrigen->docTipo->tipo === 'venta') {
  $docOrigenTipo = (object) Helpers::qry("select * from doc_tipo where nombre = 'recibo'");
} else {
  $docOrigenTipo = (object) Helpers::qry("select * from doc_tipo where nombre = 'op'");
  }
  foreach($docOrigen->valores as $valor) {
    $total+= $valor->importe;
  }
  $newDoc = new stdClass();
  $newDoc->id = -1;
  $newDoc->socio_id = $docOrigen->socio_id;
  $newDoc->user_id = $docOrigen->user_id;
  $newDoc->total = $total;
  $newDoc->doc_tipo_id = $docOrigenTipo->id;
  $newDoc->fecha = $docOrigen->fecha;
  $newDoc->numero = $docOrigenTipo->ultimo_numero+1;
  $newDoc->valores = $docOrigen->valores;
   
  $opId = graba($newDoc, false)->lastId; // el false es para que no aplique este mov, lo aplico yo abajo
  $newDoc->id = $opId;
  /*cancelados: [{
        id: '221',
        usuario: ' Tamara',
        tipo: 'factura',
        fecha: '09/03/2018',
        numero: '18',
        total: '1000.00',
        saldo: '885.00',
        'tipo_nombre': 'factura',
        'tipo_tipo': 'venta',
        dets: [{
            numero: '#18',
            nombre: '175/25/10',
            'precio_compra': '150.00',
            cantidad: '2.00',
            total: '1000.00'
        }],
        pago: '11',
        saldoNuevo: 874
    }*/
  if($opId === 0) {
    throw new Exception("no se pudo insertar el registro en la base");
  }
  $newDoc->cancelados = array();
  $newDoc->cancelados[0]["id"] = $docOrigen->id;
  $newDoc->cancelados[0]["pago"] = $total;
  aplica($newDoc);
}

function aplica($doc){
  foreach ($doc->cancelados as $destino) {
    $destino = (object) $destino;
    if(isset($destino->pago)){
      $destino = (object) $destino;
      $pago = $destino->pago;
      Helpers::qryExec("insert into doc_apl set doc_origen = $doc->id, doc_destino = $destino->id, importe = $pago");
      Helpers::qryExec("update doc set saldo = saldo-$pago where id = $doc->id");
      Helpers::qryExec("update doc set saldo = saldo-$pago where id = $destino->id");
    }
  }

}

