<?php
$productos = Helpers::qryAll(
  "SELECT p.*, m.nombre as marca
    FROM producto p
      left join marca m on m.id = p.marca_id
    where !p.borrado");
foreach ($productos as $key => $producto) {
  $productoId = $producto['id'];
  $productos[$key]['stock'] = Helpers::qryAll("
    select d.nombre as deposito, coalesce(s.cantidad, 0) as cantidad from deposito d 
      left join stock s on s.deposito_id = d.id and s.producto_id = $productoId
  ");
}
$this->resp->productos = $productos;
exit(json_encode($this->resp));
