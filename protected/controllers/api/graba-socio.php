<?php
try{
  $socio =  (object) $this->post;
  // vd2($socio);
  $tipos = !$socio->tipos ? [] :explode(',', $socio->tipos);
  $socio_id = (int)$socio->id === -1 ? false : $socio->id;
  $file = isset($_FILES['image']) ? $_FILES['image'] : false;
  if($file){
    $fileName = saveImage($file, $socio->id);
  } else if ($socio->imagen) {
    $file_parts = pathinfo($socio->imagen);
    $fileName = $file_parts['basename'];
  }

  if($socio_id) {
    try {
      $updateQuery = "
        update socio set nombre=:nombre, apellido=:apellido,
              email=:email, imagen=:imagen, domicilio=:domicilio, observaciones=:observaciones, telefono=:telefono, password=:password
        where id = :id";
      $params = array(
        'id' => $socio_id, 
        'nombre' => $socio->nombre, 
        'apellido' => $socio->apellido, 
        'domicilio' => $socio->domicilio, 
        'observaciones' => $socio->observaciones, 
        'telefono' => $socio->telefono, 
        'email' => $socio->email,
        'password' => $socio->password,
        'imagen' => $fileName);
      $resp = Helpers::qryExec($updateQuery, $params);
    } catch(Exception $e) {
      vd2('error', $e);
    }
  } else {
    try {
      $insertQuery = "
        insert into socio(nombre,apellido,email,imagen, domicilio, telefono, observaciones, password)
        values(:nombre,:apellido,:email, :imagen, :domicilio, :telefono, :observaciones, :password  )
      ";
      $params = array(
        'nombre' => $socio->nombre, 
        'apellido' => $socio->apellido, 
        'email' => $socio->email,
        'domicilio' => $socio->domicilio, 
        'observaciones' => $socio->observaciones, 
        'telefono' => $socio->telefono, 
        'password' => $socio->password, 
        'imagen' => $fileName);
        // vd2($params);
      // vd2($insertQuery, $socio, $params);
      $resp = Helpers::qryExec($insertQuery, $params);
      $socio_id = Helpers::qryScalar("
        SELECT LAST_INSERT_ID();
      ");
    } catch(Exception $e) {
      vd2('error', $e);
    }
  }
  if(count($tipos)) {
    Helpers::qryExec("
      delete from socio_socio_tipo where socio_id = $socio_id
    ");
    foreach ($tipos as $tipo) {
      Helpers::qryExec("
        insert into socio_socio_tipo(socio_id, socio_tipo_id) values($socio_id, '$tipo')
      ");
    }
  }

  // vd2($resp);
  $this->resp->response = $resp;
} catch(ExampleException $e) {
  vd2($e);  
}
exit(json_encode($this->resp));


function saveImage($file, $ownerId, $filename = false) {
  $file_parts = pathinfo($file['name']);
  $file_extension = $file_parts['extension'];
  $filename = $filename ? $filename : uniqid().'.'.$file_extension;
  $images_dir = $_SERVER['DOCUMENT_ROOT'] . '/consignaciones.api/images/';
  $base_name = basename($filename);
  $target_file = $images_dir . $base_name;
  $tmp_name = $file['tmp_name'];
  if(is_uploaded_file($tmp_name)){
    if(file_exists($target_file)){
      unlink($target_file);
    }
    $imagenAnterior = Helpers::qryScalar("select imagen from socio where id = ".$ownerId);
    if($imagenAnterior){
      if(file_exists($images_dir.'web-1600/'.$imagenAnterior)){
        unlink($images_dir.'web-1600/'.$imagenAnterior);
      }
      if(file_exists($images_dir.'web-200/'.$imagenAnterior)){
        unlink($images_dir.'web-200/'.$imagenAnterior);
      }
      if(file_exists($images_dir.'web-80/'.$imagenAnterior)){
        unlink($images_dir.'web-80/'.$imagenAnterior);
      }
    }
    try {
      //throw exception if can't move the file
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 85% -resize 80 " . $images_dir.'web-80/'.$base_name;
      exec($cmd.' 2>&1');
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 85% -resize 200 " . $images_dir.'web-200/'.$base_name;
      exec($cmd.' 2>&1');
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 85% -resize 1600 " . $images_dir.'web-1600/'.$base_name;
      exec($cmd.' 2>&1');
      // $this->resp->execStr = $cmd;
      // vd2(1);
      if (!move_uploaded_file($tmp_name, $target_file)) {
        die('Could not move file');
      }
    } catch (Exception $e) {
      die ('File did not upload: ' . $e->getMessage());
    }
  }
  return $base_name;
}