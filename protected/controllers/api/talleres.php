<?php
$division_id= $this->get['division_id'];
$nivel_id = Helpers::qryScalar("
  SELECT n.id
  FROM nivel n
    INNER JOIN anio a ON n.id = a.Nivel_id
    INNER JOIN division d ON a.id = d.Anio_id
  WHERE d.id = $division_id
");
// $anio_id = $this.get->anio;
//var_dump($this);die;
$ret = new stdClass();
$selectCats = "
  SELECT * FROM taller_categoria order by orden
";
$categorias= Helpers::qryAll($selectCats);
$selectTalleres = "
  SELECT * FROM taller where LOCATE('$nivel_id', niveles) order by orden";
$talleres = Helpers::qryAll($selectTalleres);
$selectAlumnos = "
  SELECT a.id, CONCAT(a.apellido, ', ', a.nombre) AS nombre, ad.id AS alumno_division_id
  FROM alumno a
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.Division_id = $division_id AND ad.activo
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
  order by a.apellido, a.nombre /*limit 1*/";
$alumnos = Helpers::qryAll($selectAlumnos);
$selectNotas = "
  SELECT ad.id AS alumno_division_id, tn.id AS nota_id, tn.nota, t.id AS taller_id, tn.taller_valoracion_id
  FROM alumno_division ad
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    INNER JOIN taller_nota tn ON ad.id = tn.alumno_division_id
    INNER JOIN taller t ON tn.taller_id = t.id
  WHERE ad.Division_id = $division_id AND ad.activo";
$notas = Helpers::qryAll($selectNotas);
$selectValoraciones= "
  SELECT * from taller_valoracion";
$valoraciones = Helpers::qryAll($selectValoraciones);

$this->resp->data = new stdClass();
$this->resp->data->alumnos = $alumnos;
$this->resp->data->valoraciones = $valoraciones;
$this->resp->data->categorias = $categorias;
$this->resp->data->talleres= $talleres;
$this->resp->data->notas = $notas;

exit(json_encode($this->resp));