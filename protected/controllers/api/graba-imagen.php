
<?php
$matricula = $this->post['matricula'];
// sleep(10);
$file = isset($_FILES['imagen']) ? $_FILES['imagen'] : false;
$fileName = '';
if ($file) {
    $fileName = saveImage($file, $matricula);
}
$this->resp->response = 'ok';
exit(json_encode($this->resp));

function saveImage($file, $filename)
{
    $file_parts = pathinfo($file['name']);
    $filename = $filename . '.png';
    $images_dir = '/prg/assets/fotos/';
    $base_name = basename($filename);
    $target_file = $images_dir . $base_name;
    $tmp_name = $file['tmp_name'];
    if (is_uploaded_file($tmp_name)) {
        if (file_exists($target_file)) {
            unlink($target_file);
        }
        try {
            if (!move_uploaded_file($tmp_name, $target_file)) {
                die('Could not move file');
            }
            resize($target_file);
        } catch (Exception $e) {
            die('File did not upload: ' . $e->getMessage());
        }
    }
    return $base_name;
}
function resize($target_file)
{
    try {
        //throw exception if can't move the file
        $cmd = "/prg/iae.api/resize.sh " . $target_file;
        exec($cmd . ' 2>&1');
        // $cmd = "/prg/iae.api/cp_test.sh " . $target_file;
        // exec($cmd . ' 2>&1');
    } catch (Exception $e) {
        die('File did not upload: ' . $e->getMessage());
    }
}