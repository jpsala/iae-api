<?php
$deposito =  (object) $this->post;
if($deposito->id !== '-1') {
  try {
    $updateQuery = "
      update deposito set  nombre=:nombre where id = :id";
    $params = array(
      'id' => $deposito->id, 
      'nombre' => $deposito->nombre);
      // vd2($updateQuery, $deposito, $params);
    $resp = Helpers::qryExec($updateQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
} else {
  try {
    $insertQuery = "
      insert into deposito(nombre)
        values(:nombre)
    ";
    $params = array(
      'nombre' => $deposito->nombre);
    // vd2($updateQuery, $deposito, $params);
    $resp = Helpers::qryExec($insertQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
}
// vd2($resp);
$this->resp->response = $resp;
exit(json_encode($this->resp));
