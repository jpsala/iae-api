<?php
include_once('doc.php');
$doc =  (object) $this->body;
$transaction = Yii::app()->db->beginTransaction();
$resp = graba($doc, $this->socio_id);
// ve2($resp->data);
$error = (isset($resp->error));
if(!$error){
  $transaction->commit();
  $this->resp->response = (object) $resp;
}else{
  $transaction->rollback();
  $this->resp->response = (object) ['error' => $resp->data->getMessage()];
}
  
exit(json_encode($this->resp));
