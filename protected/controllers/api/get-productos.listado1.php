<?php
$productos = Helpers::qryAllObj(
  "SELECT p.*, m.nombre as marca
    FROM producto p
      left join marca m on m.id = p.marca_id
    where !p.borrado and p.tipo = 'a'");
foreach ($productos as $key =>  $p) {
  $stock = "
    SELECT d.nombre, COALESCE(s.cantidad, 0) AS cantidad
    FROM deposito d
      LEFT JOIN stock s ON d.id = s.deposito_id and s.producto_id = $p->id
  ";
  $productos[$key]->stock = Helpers::qryAll($stock);
}
$this->resp->productos = $productos;
exit(json_encode($this->resp));
