<?php
$data = [];
$rows = Helpers::qryAllObj(
    "
  SELECT dt.signo_valor, d.id as doc_id, CONCAT(dt.nombre, '-', LPAD(d.numero, 8, '0')) AS comprob, DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') as fecha, d.id AS doc_id, d.numero,
    d.detalle, dt.operacion_tipo, d.total as pago, d.total as total
  FROM doc d
  INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id AND dt.signo_valor
  order by d.id desc  ");

// $rows = Helpers::qryAllObj(
//   "
//     SELECT
//       (SELECT GROUP_CONCAT(CONCAT(p.nombre, ' $', dd.total) SEPARATOR ' / ') FROM doc_det dd INNER JOIN producto p ON dd.producto_id = p.id WHERE dd.doc_id = d.id) AS 'producto',
//       CONCAT(dt.nombre, '-', LPAD(d.numero, 8, '0')) AS comprob, DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') AS fecha,
//       d.total AS total, da.importe AS pago,
//       (SELECT dv.importe FROM doc_valor dv WHERE dv.doc_id = da.doc_origen AND dv.doc_valor_tipo_id = (SELECT dvt.id FROM doc_valor_tipo dvt LIMIT 1)) AS efectivo,
//       (SELECT dv.importe FROM doc_valor dv WHERE dv.doc_id = da.doc_origen AND dv.doc_valor_tipo_id = (SELECT dvt.id FROM doc_valor_tipo dvt LIMIT 1,2)) AS tarjeta
//     FROM doc_apl da
//       INNER JOIN doc d ON da.doc_destino = d.id
//       INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
//     WHERE da.doc_origen IN (SELECT d.id FROM doc d INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id AND dt.signo_valor)
//   ");
foreach ($rows as $row) {
    $signo = $row->signo_valor;
    $fecha = $row->fecha;
    $tmp = new stdClass();
    $tmp = $row;
    $tmp->detalle = $row->operacion_tipo;
    if ($row->operacion_tipo !== 'f') {
        $tmp = Helpers::qryObj(
            "
        SELECT distinct da.id, $fecha as fecha,
          (SELECT GROUP_CONCAT(CONCAT(cast(dd.cantidad as unsigned), ' ', p.nombre, ' $', dd.total) SEPARATOR ' / ')
          FROM doc_det dd
            INNER JOIN producto p ON dd.producto_id = p.id
          WHERE dd.doc_id = d.id AND p.tipo = 'a') AS 'producto',
          (SELECT GROUP_CONCAT(CONCAT(cast(dd.cantidad as unsigned), ' ', p.nombre, ' $', dd.total) SEPARATOR ' / ')
            FROM doc_det dd
              INNER JOIN producto p ON dd.producto_id = p.id
            WHERE dd.doc_id = d.id AND p.tipo = 's') AS 'servicio',
            CONCAT(dt.nombre, '-', LPAD(d.numero, 8, '0')) AS comprob, DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') AS fecha,
          da.importe * $signo AS total, da.importe * $signo AS pago,
          (SELECT dv.importe FROM doc_valor dv WHERE dv.doc_id = da.doc_origen AND dv.doc_valor_tipo_id = (SELECT dvt.id FROM doc_valor_tipo dvt LIMIT 1)) AS efectivo,
          (SELECT dv.importe FROM doc_valor dv WHERE dv.doc_id = da.doc_origen AND dv.doc_valor_tipo_id = (SELECT dvt.id FROM doc_valor_tipo dvt LIMIT 1,2)) AS tarjeta
        FROM doc_apl da
          INNER JOIN doc d ON da.doc_destino = d.id
          INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
        WHERE da.doc_origen = $row->doc_id
      ");

    }
    $data[] = $tmp;
}
$arrastre = 0;
foreach ($data as $key => $row) {
    $arrastre += $row->pago;
    $data[$key]->arrastre = $arrastre;
}
$this->resp->data = $data;
$this->resp->total = $arrastre;
exit(json_encode($this->resp));
