<?php

$docDet = Helpers::qryOrEmpty(
  "SELECT d.*, p.nombre as producto, p.imagen as producto_imagen
    FROM doc_det d
      left JOIN producto p on p.id = d.producto_id
      inner join doc doc on doc.id = d.doc_id
    where d.doc_id = -1
  ");
$this->resp->docDet = $docDet;
exit(json_encode($this->resp));