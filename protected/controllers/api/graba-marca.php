<?php
$marca =  (object) $this->post;
if($marca->id !== '-1') {
  try {
    $updateQuery = "
      update marca set  nombre=:nombre where id = :id";
    $params = array(
      'id' => $marca->id, 
      'nombre' => $marca->nombre);
      // vd2($updateQuery, $marca, $params);
    $resp = Helpers::qryExec($updateQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
} else {
  try {
    $insertQuery = "
      insert into marca(nombre)
        values(:nombre)
    ";
    $params = array(
      'nombre' => $marca->nombre);
    // vd2($updateQuery, $marca, $params);
    $resp = Helpers::qryExec($insertQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
}
// vd2($resp);
$this->resp->response = $resp;
exit(json_encode($this->resp));
