<?php
$producto =  (object) $this->post;
$file = isset($_FILES['imagen']) ? $_FILES['imagen'] : false;
$fileName = '';
if($file){
  $fileName = saveImage($file, $producto->id);
} else if ($producto->imagen) {
  $file_parts = pathinfo($producto->imagen);
  $fileName = $file_parts['basename'];
}
$producto->marca_id = $producto->marca_id === 'null' ? null : $producto->marca_id;
if($producto->id !== '-1') {
  try {
    $updateQuery = "
      update producto set  
        nombre=:nombre, 
        marca_id=:marca_id, 
        precio_compra=:precio_compra,
        precio_venta=:precio_venta,
        tipo=:tipo,
        imagen=:imagen
      where id = :id";
    $params = array(
      'id' => $producto->id, 
      'nombre' => $producto->nombre, 
      'tipo' => $producto->tipo, 
      'marca_id' => $producto->marca_id, 
      'precio_compra' => $producto->precio_compra, 
      'precio_venta' => $producto->precio_venta, 
      'imagen' => $fileName);
      // vd2($updateQuery, $producto, $params);
    $resp = Helpers::qryExec($updateQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
} else {
  try {
    $insertQuery = "
      insert into producto(nombre, tipo, marca_id, precio_compra, precio_venta, imagen)
        values(:nombre, :tipo, :marca_id, :precio_compra, :precio_venta, :imagen )
    ";
    $params = array(
      'nombre' => $producto->nombre, 
      'tipo' => $producto->tipo, 
      'marca_id' => $producto->marca_id, 
      'precio_compra' => floatval($producto->precio_compra), 
      'precio_venta' => floatval($producto->precio_venta), 
      'imagen' => $fileName);
    // vd2($updateQuery, $producto, $params);
    $resp = Helpers::qryExec($insertQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
}
// vd2($resp);
$this->resp->response = $resp;
exit(json_encode($this->resp));


function saveImage($file, $ownerId, $filename = false) {
  $file_parts = pathinfo($file['name']);
  $file_extension = $file_parts['extension'];
  $filename = $filename ? $filename : uniqid().'.'.$file_extension;
  $images_dir = $_SERVER['DOCUMENT_ROOT'] . '/consignaciones.api/images/';
  $base_name = basename($filename);
  $target_file = $images_dir . $base_name;
  $tmp_name = $file['tmp_name'];
  if(is_uploaded_file($tmp_name)){
    if(file_exists($target_file)){
      unlink($target_file);
    }
    $imagenAnterior = Helpers::qryScalar("select imagen from producto where id = ".$ownerId);
    if($imagenAnterior){
      if(file_exists($images_dir.'web-1600/'.$imagenAnterior)){
        unlink($images_dir.'web-1600/'.$imagenAnterior);
      }
      if(file_exists($images_dir.'web-200/'.$imagenAnterior)){
        unlink($images_dir.'web-200/'.$imagenAnterior);
      }
      if(file_exists($images_dir.'web-80/'.$imagenAnterior)){
        unlink($images_dir.'web-80/'.$imagenAnterior);
      }
    }
    try {
      //throw exception if can't move the file
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 95% -resize 80 " . $images_dir.'web-80/'.$base_name;
      exec($cmd.' 2>&1');
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 85% -resize 200 " . $images_dir.'web-200/'.$base_name;
      exec($cmd.' 2>&1');
      $cmd = "/usr/bin/convert -verbose " . $tmp_name . " -quality 85% -resize 1600 " . $images_dir.'web-1600/'.$base_name;
      exec($cmd.' 2>&1');
      // $this->resp->execStr = $cmd;
      // vd2(1);
      if (!move_uploaded_file($tmp_name, $target_file)) {
        die('Could not move file');
      }
    } catch (Exception $e) {
      die ('File did not upload: ' . $e->getMessage());
    }
  }
  return $base_name;
}