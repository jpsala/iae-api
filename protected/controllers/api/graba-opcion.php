<?php
$opcion =  (object) $this->post;

if($opcion === '-1'){
  try {
    $insertQuery = "
      insert into opcion(nombre, valor)
        values(:nombre, :valor )
    ";
    $params = array(
      'nombre' => $opcion->nombre, 
      'valor' => $opcion->valor);
    $resp = Helpers::qryExec($insertQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
} else {
  try {
    $updateQuery = "
      update opcion set  
        nombre=:nombre, 
        valor=:valor
      where id = :id";
    $params = array(
      'id' => $opcion->id, 
      'nombre' => $opcion->nombre, 
      'valor' => $opcion->valor);
    $resp = Helpers::qryExec($updateQuery, $params);
  } catch(Exception $e) {
    vd2('error', $e);
  }
}

$status = $this->resp->status;


exit(json_encode($this->resp));
