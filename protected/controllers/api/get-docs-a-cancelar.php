<?php
$socio_id = $this->get['socio_id'];
$docs = Helpers::qryAllObj(
  "
    SELECT d.id, s.nombre_completo as usuario, dt.nombre AS tipo, DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') AS fecha, d.numero, 
            dt.signo_cc * d.total as total,  dt.signo_cc * d.saldo as saldo, dt.nombre AS tipo_nombre, dt.tipo AS tipo_tipo
    FROM  doc d 
      left JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
      left JOIN socio s ON s.id = d.user_id
    where d.socio_id = $socio_id and dt.signo_cc = 1 and d.saldo > 0
    order by d.fecha_creacion
  ");
foreach ($docs as $key => $doc) {
  // vd2($docs[$key]);
  if(!isset($docs[$key]->dets)) $docs[$key]->dets = [];
  if($doc->tipo_nombre === 'rendicion'){
    $docs[$key]->dets = Helpers::qryAll("
    SELECT CONCAT('#', d.numero) AS numero, p.nombre, detcompra.precio_compra, dr.cantidad, detcompra.precio_compra * dr.cantidad AS total
    FROM doc d
    INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id AND dt.nombre = 'rendicion'
    INNER JOIN doc_rendir dr ON d.id = dr.doc_rendicion_id
    INNER JOIN doc_det detventa ON dr.doc_det_venta = detventa.id
    INNER JOIN doc_det detcompra ON dr.doc_det_compra = detcompra.id
    INNER JOIN producto p ON detcompra.producto_id = p.id
    WHERE dr.doc_rendicion_id = $doc->id
    order by d.fecha_creacion
    ");
  }else if ($doc->tipo_nombre === 'op') {
    $docs[$key]->dets = Helpers::qryAll("
      SELECT concat('#', d.numero) AS numero/*,  DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') AS fecha */, d.total, da.importe, '' as precio_compra, 
        (
        SELECT CONCAT(SUBSTR(GROUP_CONCAT(CONCAT(dd.cantidad, ' ', p.nombre)), 1 , 155),'...') 
          FROM doc_det dd 
            INNER JOIN doc d ON dd.doc_id = d.id
            INNER JOIN producto p ON dd.producto_id = p.id) AS nombre, '' as cantidad
        FROM doc_apl da
          INNER JOIN doc d ON da.doc_destino = d.id
        WHERE da.doc_origen = $doc->id");
  }else{
    $docs[$key]->dets = Helpers::qryAll("
      SELECT CONCAT('#', d.numero) AS numero, p.nombre, dd.precio_compra, dd.cantidad, dd.total
      FROM  doc d 
        inner JOIN doc_det dd ON d.id = dd.doc_id
        inner JOIN producto p ON dd.producto_id = p.id
      where d.id = $doc->id
      order by d.fecha_creacion
    ");
  }
}
$saldo = Helpers::qryScalar("select saldo($socio_id)");
$this->resp->docs = $docs;
$this->resp->saldo = $saldo;
exit(json_encode($this->resp));