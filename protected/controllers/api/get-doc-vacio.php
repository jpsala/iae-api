<?php
//doc
$doc = Helpers::qryOrEmpty(
  "SELECT d.*
    FROM doc d where d.id = -1
  ");
$doc["user_id"] = $this->socio_id;
$doc["fecha"] = date('d/m/Y');
$doc["det"] = [];
$doc["val"] = [];
$this->resp->doc = $doc;
//docValor
$docValor = Helpers::qryOrEmpty(
  "SELECT d.*
    FROM doc_valor d where d.id = -1
  ");
$this->resp->docValor = $docValor;
//docDet
$docDet = Helpers::qryOrEmpty(
  "SELECT d.*
    FROM doc_det d 
    where d.id = -1
  ");
  // vd2($docDet);
$this->resp->docDet = $docDet;

exit(json_encode($this->resp));