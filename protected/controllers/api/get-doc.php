<?php
$doc_id = isset($this->get['id']) ? $this->get['id'] : -1;
$doc = Helpers::qry(
  "SELECT d.*, dt.nombre as tipo, DATE_FORMAT(d.fecha, \"%d/%m/%Y\") as fecha, s.nombre_completo as socio, s.imagen as socio_imagen
    FROM doc d 
      left JOIN socio s on s.id = d.socio_id
      inner join doc_tipo dt on dt.id = d.doc_tipo_id
    where d.id = $doc_id      
  ");
$docDet = Helpers::qryAll(
  "SELECT d.*, p.nombre as producto, p.imagen as producto_imagen
    FROM doc_det d
      left JOIN producto p on p.id = d.producto_id
      inner join doc doc on doc.id = d.doc_id
    where d.doc_id = $doc_id
  ");
$docValor = Helpers::qryAll(
  "SELECT d.*
    FROM doc_valor d where d.doc_id = $doc_id
  ");
$doc['det'] = $docDet or [];
$doc['val'] = $docValor or [];
$this->resp->doc = $doc;
exit(json_encode($this->resp));