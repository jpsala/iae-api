<?php

function vd2() {
  echo '<pre>';
  v2(func_get_args());
  echo '</pre>';
  Yii::app()->end();
}

function v2($v) {
  foreach (func_get_args() as $v) {
    foreach ($v as $value) {
      // CVarDumper::dump($value, 100, true);
      echo print_r($value, 1);
    }
  }
}

function ve() {
  v(func_get_args());
//    $args = func_get_args();
//    echo "<pre>";
//    var_export($args);
//    echo "</pre>";
}

function ve2() {
  echo '<pre>';
  v2(func_get_args());
  echo '</pre>';
}

function p() {
  $args = func_get_args();
  foreach ($args as $a) {
    echo $a . " . ";
  }
  echo "<br/>";
}

class Helpers {

  static private $_host;

  static public function execInTry($func) {
    try {
      return $func();
    } catch (PDOException $e) {
      echo 'Custom Error Message';

      // Output the error code and the error message
      echo "<pre>";
      print_r($e->getCode() . '-' . $e->getMessage());
      echo "</pre>";
      die;
    } catch (Exception $e) {
      print_r($e);
      die;
    }
  }

  /**
   * <pre>
   * Ejecuta
   * Yii::app()
   *  ->db
   *  ->createCommand($qry)
   *  ->query();
   * dentro de un try
   * </pre>
   * @param type $qry
   * @param array $params
   */
  public static function qryFields($s) {
    $qry = Yii::app()->db->pdoInstance->query($s);
    foreach (range(0, $qry->columnCount() - 1) as $column_index) {
      $meta = $qry->getColumnMeta($column_index);
      $fields[$meta["name"]] = "";
    }
    return $fields;
  }

  public static function qryFieldsAndData($s) {
    $qry = Yii::app()->db->pdoInstance->query($s);
    foreach (range(0, $qry->columnCount() - 1) as $column_index) {
      $meta = $qry->getColumnMeta($column_index);
      $fields[] = $meta["name"];
    }
    $data = $qry->setFetchMode(PDO::FETCH_NUM);
    $data = $qry->fetchAll();
    return array("fields" => $fields, "data" => $data);
  }

  public static function qryDataRow($s) {
    $qry = Yii::app()->db->pdoInstance->query($s);
    $qry->setFetchMode(PDO::FETCH_NUM);
    return $qry->fetch();
  }

  public static function qryDataAll($s) {
    $qry = Yii::app()->db->pdoInstance->query($s);
    $data = $qry->setFetchMode(PDO::FETCH_NUM);
    $data = $qry->fetchAll();
    return $data;
  }

  public static function qryScalar($qry, $params = array()) {
    try {
      return Yii::app()->db->createCommand($qry)->queryScalar($params);
    } catch (Exception $e) {
      echo "<pre>";
      //if ($e->getCode() == 42) {
      echo 'Error ejecutando query:</br>';
      ve($e->getCode() . '-' . $e->getMessage());
      //}
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    return null;
  }

  public static function qryOrEmpty($s) {
    $qry = Yii::app()->db->pdoInstance->query($s);
    $qry->setFetchMode(PDO::FETCH_ASSOC);
    $data = $qry->fetch();
    if ($qry->rowCount() == 0) {
      foreach (range(0, $qry->columnCount() - 1) as $column_index) {
        $meta = $qry->getColumnMeta($column_index);
        $fields[$meta["name"]] = "";
      }
      return $fields;
    } else {
      return $data;
    }

    return $fields;
  }

  public static function qry($qry, $params = array()) {
    try {
      return Yii::app()->db->createCommand($qry)->query($params)->read();
    } catch (Exception $e) {
      echo "<pre>";
      //if (in_array($e->getCode(),array(42,42000))) {
      echo 'Error ejecutando query:</br>';
      ve($e->getCode() . '-' . $e->getMessage());
      //}
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    return null;
  }

  public static function qryObj($qry, $params = array(), $orEmpty = false) {
    $qry = Yii::app()->db->pdoInstance->query($qry);
    $qry->setFetchMode(PDO::FETCH_OBJ);
    try {
      $data = $qry->fetch();
    } catch (Exception $e) {
      echo "<pre>";
      echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
      ve($e->getMessage());
//      }
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    if ($qry->rowCount() == 0 and $orEmpty) {
      foreach (range(0, $qry->columnCount() - 1) as $column_index) {
        $meta = $qry->getColumnMeta($column_index);
        $fields[$meta["name"]] = "";
      }
      return array($fields);
    } else {
      return $data;
    }
  }

  public static function qryAllOrEmpty($qry, $params = array()) {
    $qry = Yii::app()->db->pdoInstance->query($qry);
    $qry->setFetchMode(PDO::FETCH_ASSOC);
    try {
      $data = $qry->fetchAll();
    } catch (Exception $e) {
      echo "<pre>";
      echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
      ve($e->getMessage());
//      }
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    if ($qry->rowCount() == 0) {
      foreach (range(0, $qry->columnCount() - 1) as $column_index) {
        $meta = $qry->getColumnMeta($column_index);
        $fields[$meta["name"]] = "";
      }
      return array($fields);
    } else {
      return $data;
    }
  }

  public static function qryAll($qry, $params = array(), $conn = null) {
    $conn = $conn ? $conn : Yii::app()->db;
    try {
      $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
      return $conn->createCommand($qry)->queryAll(true, $params);
    } catch (Exception $e) {
      echo "<pre>";
      echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
      ve($e->getMessage());
//      }
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    return null;
  }

  public static function qryAllObj($qry, $params = array(), $orempty = false) {
    try {
      $qry = Yii::app()->db->pdoInstance->query($qry);
      $qry->setFetchMode(PDO::FETCH_OBJ);
      $data = $qry->fetchAll();
    } catch (Exception $e) {
      echo "<pre>";
      echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
      ve($e->getMessage());
//      }
      echo $e->getTraceAsString();
      echo "</pre>";
      die;
    }
    if ($qry->rowCount() == 0 and $orempty) {
      $fields = new stdClass();
      foreach (range(0, $qry->columnCount() - 1) as $column_index) {
        $meta = $qry->getColumnMeta($column_index);
        $fields->$meta["name"] = "";
      }
      return array($fields);
    } else {
      return $data;
    }
  }

  public static function qryExec($qry, $params = array(), $conn = null) {
    $conn = $conn ? $conn : Yii::app()->db;
    return $conn->createCommand($qry)->execute($params);
//			try {
//				return Yii::app()->db->createCommand($qry)->execute($params);
//			} catch (Exception $e) {
//				echo "<pre class=\"word-wrap\">";
//				if ($e->getCode() == 42000 or $e->getCode() == 42) {
//					echo 'Error ejecutando query:</br>';
//					ve($e->getCode() . ' -> <textarea style="height: 211px;width: 571px;display:block">' . $e->getMessage() . "</textarea>");
//				}
//				echo $e->getTraceAsString();
//				echo "</pre>";
//				die;
//			}
  }

  public static function clearAttributes($attributes) {
    $cleanArray = array();
    foreach ($attributes as $key => $value) {
      $cleanArray[$key] = ($value == 'null') ? null : $value;
    }
    return $cleanArray;
  }

  public function round2($val) {
    $partes = explode(".", $val);
    $dec = $partes[1];
    if ($dec < 50) {
      $dec = 50;
    } elseif ($dec > 50) {
      $dec = 50;
    }
  }

  public static function getHostName() {
    if (!self::$_host) {
      self::$_host = gethostbyaddr("127.0.0.1");
    }
    return self::$_host;
  }

  public function muestraYVuelve($texto = "Por hacer :)") {
    $url = Yii::app()->getBaseUrl();
    echo "<span>" . $texto . "</span>";
    echo <<< END
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

        <script type="text/javascript">
            window.setTimeout(function(){window.location = "$url"},200000);
    </script>
END;
  }

  public static function fechaParaGrabar($fecha, $format = "Y/m/d") {
    return date($format, mystrtotime(str_replace('/', '.', $fecha)));
  }

  public static function date($format = "Y/m/d", $fecha) {
    return date($format, mystrtotime(str_replace("/", ".", $fecha)));
  }

  public static function error($mensaje, $data = null, $mandaMail = false) {
    require Yii::app()->basePath . '/controllers/HelpersController.php';
    $_GET["mensaje"] = $mensaje;
    $_GET["data"] = $data;
    $c = new HelpersController("error");
    $c->run('error');
  }

  public static function getTable($modelName, $data, $colsGeneral = array(), $options = array()) {
    $viewFile = Yii::app()->getViewPath() . DIRECTORY_SEPARATOR . "exec" . DIRECTORY_SEPARATOR . "table.php";
    ob_start();
    ob_implicit_flush(false);
    require($viewFile);
    return ob_get_clean();
  }

  public function getTable4PDF($modelName, $data, $cols = array(), $filter = true, $borders = true) {
    $dataForView = array("modelName" => $modelName, "data" => $data, "cols" => $cols, "borders" => $borders, "Filter" => $filter);
    extract($dataForView, EXTR_PREFIX_SAME, 'data');
    $viewFile = Yii::app()->getViewPath() . DIRECTORY_SEPARATOR . "exec" . DIRECTORY_SEPARATOR . "table4PDF.php";
    ob_start();
    ob_implicit_flush(false);
    require($viewFile);
    return ob_get_clean();
  }

  public static function getDiv($modelName, $data, $cols = array(), $fieldSets = array(), $borders = true) {
    $dataForView = array("modelName" => $modelName, "data" => $data, "cols" => $cols, "fieldSets" => $fieldSets);
    extract($dataForView, EXTR_PREFIX_SAME, 'data');
    $viewFile = Yii::app()->getViewPath() . DIRECTORY_SEPARATOR . "exec" . DIRECTORY_SEPARATOR . "gen.php";
    ob_start();
    ob_implicit_flush(false);
    require($viewFile);
    return ob_get_clean();
  }

  public static function envia_mail($tipo, $host, $puerto, $usuario, $password, $asunto, $cuerpo, $from, $to) {

    $SM = Yii::app()->swiftMailer;

    $mailHost = $host;
    if ($tipo == 'smtp') {
//      $mailPort = $puerto;
      $Transport = $SM->smtpTransport($mailHost)
        ->setUsername($usuario)
        ->setPassword($password)
      ;
    } else {
      $Transport = $SM->sendmailTransport($mailHost, 587, "ssl");
    }

    $Mailer = $SM->mailer($Transport);

    $Message = $SM
      ->newMessage($asunto)
      ->setFrom(array($from => $from))
      ->setTo(array($to => $to))
      //->addPart($content, 'text/html')
      ->setBody($cuerpo);

    if ($Mailer->send($Message)) {
      echo 'enviado';
    } else {
      echo 'fallo al enviar';
    }
  }

  public static function getDiasHabiles($fechainicio, $dias) {
    $diasferiados = array(
      //los que quedan del 2013, ver de cargar y recuperar los próximos
      '19-08', //  San Martín
      '14-10', //  Raza
      '25-11', //  Soberanía
      '8-12', //  Virgen
      '25-12', //  Navidad
    );
    //echo("Dias: $dias");
    //echo("\n");
    $fechainicio = mystrtotime(Helpers::fechaParaGrabar($fechainicio));
    // Incremento en 1 dia
    $diainc = 24 * 60 * 60;
    $dia = $fechainicio;
    $numero_dia = date("w", $dia);
    $i = 0;
    While ($i < $dias) {
      $dia = $dia + $diainc;
      $numero_dia = date("w", $dia);
      // si no es sábado o domingo y no esta en $feriados suma un día hábil
      if ((($numero_dia != '0') && ($numero_dia != '6')) && (!in_array(date('d-m', $dia), $diasferiados))) {
        $i += 1;
      }

      //echo(" Cantidad: $i Numero de dia: $numero_dia Día:". date("d/m/Y",$dia));
      //echo("\n");
      //die;
    }
    return date("d/m/Y", $dia);
  }

  public static function fechaDiaAnterior($fecha, $returnFormat = "Y/m/d") {
    return date($returnFormat, mystrtotime($fecha) - 1);
  }

  public static function fechaUltimoSegundo($fecha, $formato = "Y/m/d") {
    return date($formato, mystrtotime(str_replace("/", ".", $fecha))) . " 23:59:59";
  }

  public static function validateDates($obj) {
    foreach ($obj->metadata->tableSchema->columns as $columnName => $column) {
      if (in_array($column->dbType, array('date', "datetime"))) {
        $fecha = $obj->$columnName;
        if (in_array(str_replace("/", ".", $fecha), array("1969.12.31", "31.12.1969", "69.12.31", "31.12.69"))) {
          $obj->addError("$columnName", "Fecha erronea $fecha");
        } else {
          $empty = (mystrtotime($fecha) === false);
          //ve($columnName, $fecha, $empty, mystrtotime($fecha), date(mystrtotime($fecha)));
          if (($empty and $column->allowNull) or is_object($obj->$columnName)) {
            continue;
          }
          if ($empty and ! $column->allowNull and ! is_object($obj->$columnName)) {
            $obj->addError("$columnName", "No puede ser vacia o no puede grabarse $fecha");
          } else {
            $format = ($column->dbType == "date") ? "Y/m/d" : "Y/m/d h:i:s";
            $fecha = date($format, mystrtotime($fecha));
            if ($fecha == "31/12/1969" or $fecha == "1969/12/31") {
              $fecha = str_replace('/', '.', $obj->$columnName);
              if ($fecha == "31.12.1969" or $fecha == "1969.12.31") {
                $obj->addError("$columnName", "No puede ser vacia $fecha");
              } else {
                $empty = (mystrtotime(Helpers::fechaParaGrabar($fecha)) === false);
                if ($empty) {
                  $obj->addError("$columnName", "No puede ser vacia $fecha");
                  $obj->$columnName = $fecha;
                }
              }
            }
          }
        }
      }
    }
    return ($obj->errors ? false : true);
  }

  public static function transformaFecha($obj) {
    foreach ($obj->metadata->tableSchema->columns as $columnName => $column) {
      if (in_array($column->dbType, array('date', "datetime"))) {
        if (strpos($obj->$columnName, "/")) {
          $fecha = str_replace('/', '.', $obj->$columnName);
        } else {
          $fecha = $obj->$columnName;
        }
        $time = mystrtotime($fecha);
        $empty = ($time === false);
        if (($empty and $column->allowNull) or is_object($obj->$columnName)) {
          continue;
        }
        if ($empty and ! $column->allowNull and ! is_object($obj->$columnName)) {
          $obj->addError("$columnName", "No puede estar vacia ");
        }
        if (strpos($obj->$columnName, "/")) {
          $fecha = date("Y/m/d", mystrtotime(str_replace('/', '.', $obj->$columnName)));
        }
        ve($fecha);
        $format = ($column->dbType == "date") ? "Y/m/d" : "Y/m/d h:i:s";
        $fecha = date($format, mystrtotime($fecha));
        if ($fecha == "31/12/1969" or $fecha == "1969/12/31") {
          $fecha = str_replace('/', '.', $obj->$columnName);
          if ($fecha == "31.12.1969" or $fecha == "1969.12.31") {
            $obj->addError("$columnName", "No puede ser vacia $fecha");
          } else {
            $obj->$columnName = $fecha;
          }
        }
      }
    }
    return ($obj->errors ? false : true);
  }

  public static function excel($rows, $cols, $fileName) {
    ini_set('memory_limit', '-1');
    $objPHPExcel = new MyPHPExcel();
    $FIRST_CELL = "A1";
    $as = $objPHPExcel->setActiveSheetIndex(0);
    Col::init();
    foreach ($cols as $fld => $data) {
      $title = isset($data["title"]) ? $data["title"] : ucfirst($fld);
      $col = Col::next();
      $as->SetCellValue($col . '1', $title);
    }
    $as->setAutoFilter("$FIRST_CELL:" . $col . "4");
    $as->getStyle("$FIRST_CELL:" . $col . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $as->getStyle("$FIRST_CELL:" . $col . "1")->getFont()->setBold(true);
    $as->getStyle("$FIRST_CELL:" . $col . "1")->getFill()->getStartColor()->setARGB("666666");
    $as->getStyle("$FIRST_CELL:" . $col . "1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    $rowNum = 2;
    //Col::test();
    //die;
    foreach ($rows as $row) {
      Col::init();
      foreach ($cols as $fld => $data) {
        $scol = Col::next();
        //ve($cols, $scol);
        if (isset($cols[$fld]["format"]) and strtoupper($cols[$fld]["format"]) == "DATE") {
          $row[$fld] = date("d/m/Y", mystrtotime($row[$fld]));
        }
        $as->SetCellValue($scol . $rowNum, $row[$fld]);
      }
      $rowNum++;
    }
    //die;
    $lastRow = $rowNum;
    Col::init();
    foreach ($cols as $fld => $data) {
      $scol = Col::next();
      $as->getColumnDimension($scol)->setAutoSize(true);
      if (isset($data["align"])) {
        if (strtoupper($data["align"]) == "RIGHT") {
          $as->getStyle($scol . "2:" . $scol . "$lastRow")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        } elseif (strtoupper($data["align"]) == "LEFT") {
          $as->getStyle($scol . "2:" . $scol . "$lastRow")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }
      }
      if (isset($data["format"])) {
        if (strtoupper($data["format"]) == "TEXT") {
          $as->getStyle($scol . "2:" . $scol . "$lastRow")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        } elseif (strtoupper($data["format"]) == "NUMBER") {
          $as->getStyle($scol . "2:" . $scol . "$lastRow")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        } elseif (strtoupper($data["format"]) == "DATE") {
          $as->getStyle($scol . "2:" . $scol . "$lastRow")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
        }
      }
      if (isset($data["total"]) and $data["total"]) {
        $as->getStyle($scol . "2:" . $scol . "$lastRow")->getNumberFormat()->setFormatCode('#,##0.00');
      }
      if (isset($data["total"]) and $data["total"]) {
        $as->setCellValue("A$lastRow", "Totales");
        $as->setCellValue("$scol$lastRow", "=Sum(" . $scol . "2:" . $scol . ($lastRow - 1) . ")");
      }
    }
    try{
      $objPHPExcel->output($fileName);
    } catch (Exception $e) {
      vd2('error', $e);
    }
  }

  public static function edad($fecha_nacimiento) {
    list($anio, $mes, $dia) = explode("-", $fecha_nacimiento);
    $mes_actual = date("m");
    $dia_actual = date("d");
    //Toma la edad al 30/06
    if (($mes_actual > 6)) {
      $mes_actual = 6;
      $dia_actual = 30;
    }
    $anio_dif = date("Y") - $anio;
    $mes_dif = $mes_actual - $mes;
    $dia_dif = $dia_actual - $dia;
    if ($mes_dif < 0)
      $anio_dif--;
    return $anio_dif;
  }

  public static function lastInsertedId() {
    $pdo = Yii::app()->db->pdoInstance;
    return $pdo->lastInsertId();
  }

  public static function sessionSecondsLeft() {
    if (Yii::app()->user->keepSession) {
      return 14 * 24 * 60 * 60;
    } else {
      return ini_get('session.gc_maxlifetime');
    }
  }

}

function ld($v, $depth = 10) {
  l($v, $depth);
  Yii::app()->end();
}

function l($v, $depth = 10) {
  echo Yii::trace(CVarDumper::dumpAsString($v, $depth), "vardump");
}

function d($data, $highlight = true) {
  CVarDumper::dump($data, 10, $highlight);
}

function dd($data, $highlight = true) {
  CVarDumper::dump($data, 10, $highlight);
  die;
}

function v($v) {
  foreach (func_get_args() as $v) {
    foreach ($v as $value) {
      // CVarDumper::dump($value, 100, true);
      var_dump($value);
    }
  }
}

function vd() {
  v(func_get_args());
  Yii::app()->end();
}

function format_cuit($cuit) {
  if (strpos($cuit, '-')) {
    return $cuit;
  }
  $cuit_formateado = substr($cuit, 0, 2).'-'.substr($cuit,2,8).'-'.substr($cuit,-1);
  return $cuit_formateado;
}

function mystrtotime($time, $now = null) {
  // return strtotime($time);
  return strtotime(str_replace("/", "-", $time), $now);
}

class Excel extends stdClass {

  public $objPHPExcel, $nextRow, $lastRow, $lastCol, $rows, $cols, $fileName;
  public $extraData, $orientation, $as, $firstRow, $fitToPage, $options;

  public function __construct($rows, $cols, $fileName, $extraData = array(), $options = array()) {
    ini_set('memory_limit', '-1');
    $this->objPHPExcel = new MyPHPExcel();
    $this->as = $this->objPHPExcel->setActiveSheetIndex(0);
    $pageMargins = $this->as->getPageMargins();
    ini_set('memory_limit', '-1');
    $defaults = array(
      "paperSize" => "A4",
      "orientation" => "P",
      "fitToPage" => false,
      "marginTop" => .5,
      "marginLeft" => .5,
      "marginBottom" => .5,
      "marginRight" => .5,
      "fitToWidth" => null,
      "fitToHeight" => null,
    );
    $this->options = array_merge($defaults, $options);
    if (strtoupper($this->options["paperSize"]) == "A4") {
      
    } elseif (in_array(strtoupper($this->options["paperSize"]), array("LEGAL", "OFICIO"))) {
      $this->options["paperSize"] = PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL;
    } elseif (in_array(strtoupper($this->options["paperSize"]), array("LETTER", "CARTA"))) {
      $this->options["paperSize"] = PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER;
    } else {
      $this->options["paperSize"] = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4;
    }
    $this->options["orientation"] = $this->options["orientation"] == "P" ? PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT : PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE;
    $this->extraData = $extraData;
    $this->fileName = $fileName;
    $pageMargins->setTop($this->options["marginTop"] / 2.54);
    $pageMargins->setBottom($this->options["marginBottom"] / 2.54);
    $pageMargins->setLeft($this->options["marginLeft"] / 2.54);
    $pageMargins->setRight($this->options["marginRight"] / 2.54);
    if (is_numeric($this->options["fitToWidth"])) {
      $this->as->getPageSetup()->setFitToWidth($options["fitToWidth"]);
    }
    if (is_numeric($this->options["fitToHeight"])) {
      $this->as->getPageSetup()->setFitToHeight($options["fitToHeight"]);
    }

// margin is set in inches (0.5cm)

    $this->rows = $rows;
    $this->cols = $cols;
    $this->nextRow = 1;
  }

  public function run() {
    $this->printTitle();
    $FIRST_CELL = "A$this->nextRow";
    $this->firstRow = $this->nextRow;
    Col::init();
    foreach ($this->cols as $fld => $data) {
      if (isset($this->cols[$fld]["visible"]) and ! $this->cols[$fld]["visible"]) {
        continue;
      }
      $title = isset($data["title"]) ? $data["title"] : ucfirst($fld);
      $col = Col::next();
      $this->as->SetCellValue($col . $this->nextRow, $title);
      $width = isset($this->cols[$fld]["width"]) ? $this->cols[$fld]["width"] : null;
      if ($width) {
        $this->as->getColumnDimension($col)->setWidth($width);
      } else {
        $this->as->getColumnDimension($col)->setAutoSize(true);
      }
    }
    $this->lastCol = $col;
    $this->as->setAutoFilter("$FIRST_CELL:" . $col . "$this->nextRow");
    $this->as->getStyle("$FIRST_CELL:" . $col . "$this->nextRow")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $this->as->getStyle("$FIRST_CELL:" . $col . "$this->nextRow")->getFont()->setBold(true);
    $this->as->getStyle("$FIRST_CELL:" . $col . "$this->nextRow")->getFill()->getStartColor()->setARGB("666666");
    $this->as->getStyle("$FIRST_CELL:" . $col . "$this->nextRow")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    $rowNum = $this->nextRow + 1;
    foreach ($this->rows as $row) {
      Col::init();
      foreach ($this->cols as $fld => $data) {
        if (isset($this->cols[$fld]["visible"]) and ! $this->cols[$fld]["visible"]) {
          continue;
        }
        $scol = Col::next();
        if (isset($this->cols[$fld]["format"]) and strtoupper($this->cols[$fld]["format"]) == "DATE") {
          $row[$fld] = date("d/m/Y", mystrtotime($row[$fld]));
        }
        $this->as->SetCellValue($scol . $rowNum, $row[$fld]);
      }
      $rowNum++;
    }
    $this->lastRow = $rowNum - 1;
    Col::init();
    foreach ($this->cols as $fld => $data) {
      if (isset($this->cols[$fld]["visible"]) and ! $this->cols[$fld]["visible"]) {
        continue;
      }
      $scol = Col::next();
      if (isset($data["align"])) {
        if (strtoupper($data["align"]) == "RIGHT") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        } elseif (strtoupper($data["align"]) == "LEFT") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        } elseif (strtoupper($data["align"]) == "CENTER") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
      }
      if (isset($data["format"])) {
        if (strtoupper($data["format"]) == "TEXT") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        } elseif (strtoupper($data["format"]) == "NUMBER") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        } elseif (strtoupper($data["format"]) == "DATE") {
          $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
        }
      }
      if (isset($data["total"]) and $data["total"]) {
        $this->as->getStyle($scol . "2:" . $scol . $this->lastRow + 1)->getNumberFormat()->setFormatCode('#,##0.00');
      }
      if (isset($data["total"]) and $data["total"]) {
        $this->as->setCellValue("A" . ($this->lastRow + 1), "Totales");
        $this->as->setCellValue($scol . ($this->lastRow + 1), "=Sum($scol$this->firstRow:" . $scol . ($this->lastRow + 1 - 1) . ")");
      }
    }
  }

  public function printTitle() {
    
  }

  public function output() {
    $ps = $this->as->getPageSetup();
    $ps->setOrientation($this->options["orientation"]);
    $ps->setPaperSize($this->options["paperSize"]);
    if ($this->options["fitToPage"]) {
      $ps->setFitToPage(true);
    }
    //$ps->setFitToWidth(1);
    //$ps->setFitToHeight(0);
    $this->objPHPExcel->output($this->fileName);
  }

}

Class Timer {

  function startTimer() {
    global $starttime;
    $mtime = microtime();
    $mtime = explode(' ', $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $starttime = $mtime;
  }

  function endTimer() {
    global $starttime;
    $mtime = microtime();
    $mtime = explode(' ', $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $endtime = $mtime;
    $totaltime = round(($endtime - $starttime), 5);
    return $totaltime;
  }

}

Class Col {

  private static $i, $i2, $pre;
  public static $val;

  public static function init() {
    self::$pre = "";
    self::$i2 = self::$i = 0;
    self::$val = "A";
    self::next();
  }

  public static function test() {
    self::init();
    for ($index = 0; $index < 4000; $index++) {
      echo self::next() . "<br/>";
    }
  }

  public static function next() {
    $ant = self::$val;
    if (self::$i > 25) {
      self::$i = 0;
      self::$pre = chr(65 + self::$i2++);
//			ve(self::$pre);
    }
    self::$i++;
    self::$val = self::$pre . chr(64 + self::$i);
    return $ant;
  }

}
