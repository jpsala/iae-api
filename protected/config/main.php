<?php
// vd2(gethostbyaddr("127.0.0.1"));
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
if (in_array(gethostbyaddr("127.0.0.1"), array("localhost", "localhost.localdomain"))) {
    $passBase = "lani0363";
    $baseFirebird = '192.168.2.254:/export/sistema/data/iae.gdb';
    $connString = 'mysql:host=127.0.0.1;dbname=iae-nuevo';
} elseif (
      in_array(gethostbyaddr("127.0.0.1"),
      array("jp-PC","jp-note"))){
    if (isset($_GET["base"])) {
          $passBase = "lani0363";
          $baseFirebird = '192.168.2.254:/export/sistema/data/iae.gdb';
          $connString = 'mysql:host=localhost;dbname=iae-nuevo';
    } else {
        //$connString = 'mysql:host=127.0.0.1;dbname=consignaciones';
        $connString = 'mysql:host=127.0.0.1;dbname=iae-nuevo';
        $passBase = "lani0363";
        $baseFirebird = 'd:\prg\iae.fdb';
    }
} elseif (in_array(gethostbyaddr("127.0.0.1"), array('iaeg'))){
        // $connString = 'mysql:host=iae.dyndns.org;dbname=consignaciones';
        $connString = 'mysql:host=localhost;dbname=iae-nuevo';
        $passBase = "lani0363";
} else {
  vd2('no hay ninguna base configurada para '. gethostbyaddr('127.0.0.1'));
}
//$connString = 'mysql:host=127.0.0.1;dbname=iae-d:\\20141111-1101.bz2';
//$connString = 'mysql:host=127.0.0.1;dbname=consignaciones';
//	$connString = 'mysql:host=192.168.2.1;dbname=consignaciones';
//$connStringIAE = 'mysql:host=iae.dyndns.org;dbname=consignaciones';

return array(
    'language' => 'es',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'IAE',
    // preloading 'log' component
// autoloading model and component classes
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*', // giix components
        'zii.widgets.jui.CJuiWidget',
        'ext.yii-debug-toolbar.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'lani0363',
            'generatorPaths' => array(
                'ext.giix-core', // giix generators
            ),
            'ipFilters' => false,
        ),
        'giix' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'lani0363',
        ),
        'rights' => array(
//            'install' => true,
            'userNameColumn' => 'login',
            'superuserName' => 'Admin',
            'authenticatedName' => 'Authenticated',
            "userClass" => "User",
        ),
    ),
    // application components
    'components' => array(
//					'session' => array(
//							'class' => 'CHttpSession',
//							'class' => 'system.web.CDbHttpSession',
//							'connectionID' => 'db',
//							'sessionTableName' => 'session',
//							'timeout' => 60*60*10000,
//							"autoCreateSessionTable" => false,
//							'cookieMode' => 'on',
//							'cookieParams' => array(
//									'httponly' => true, // note that "O" is lowercase
//							),
//					),
        'JWT' => array(
                'class' => 'ext.jwt.JWT',
                'key' => 'JWT-SECRET-KEY-IAE',
            ),
        'session' => array(
            'sessionName' => 'SiteSession',
            'class' => 'CHttpSession',
//							'useTransparentSessionID' => ($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'true',
            'cookieMode' => 'only',
//							'timeout' => ,
            'timeout' => 60 * 60 * 1.5, // 90 min
        ),
        'coreMessages' => array(
            'basePath' => null,
        ),
        'Metadata' => array(
            'class' => 'Metadata'
        ),
        'locale' => array(
            'id' => 'ES_ar',
        ),
        'fb' => array(
            'class' => 'FB',
            'host' => $baseFirebird,
            'ibase_charset' => 'UTF8',
//                        'ibase_charset' => 'win1252',
            'usuario' => "sysdba",
            'pass' => "masterkey",
        ),
//        'includeTheme' => array(
//            'class' => 'IncludeTheme',
//            'theme' => 'aristo',
//            'themes' => array(
//                'aristo' => 'jquery-ui-1.8.7.custom.css',
//            )
//        ),
        'mail' => array(
                "class" => "Mail",
        ),
        'clientScript' => array(
            'class' => 'CClientScript',
            'scriptMap' => array(
                'jquery.js' => false,
                'jquery-ui.min.js' => false,
                'jquery-ui.css' => false,
            ),
            'coreScriptPosition' => CClientScript::POS_END,
        ),
// uncomment the following to enable URLs in path-format
        'urlManager' => array(
//            'showScriptName' => false,
//            'urlFormat' => 'path',
//            'rules' => array(
//                "" => "nota",
//          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
//          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
//            ),
        ),
        'db' => array(
            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
// 'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
            'connectionString' => $connString,
            'charset' => 'UTF8',
            'initSQLs' => array("SET session time_zone = '-3:00'","SET lc_time_names = 'es_AR'"),
            'username' => 'root',
            'password' => $passBase,
//            'password' =>'root',
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        'dbIAE' => array(
            'class' => 'CDbConnection',
           'connectionString' => 'mysql:host=localhost;dbname=consignaciones',
            // 'connectionString' => $connString,
            'charset' => 'UTF8',
            'initSQLs' => array("SET session time_zone = '-3:00';","SET lc_time_names = 'es_AR'"),
            'username' => 'root',
            'password' => $passBase,
//            'password' =>'root',
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        'dbbk' => array(
            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
            'connectionString' => "mysql:host=127.0.0.1;dbname=iae-20140219",
            'charset' => 'UTF8',
            'initSQLs' => array("SET session time_zone = '-3:00';"),
            'username' => 'root',
            'password' => "lani0363",
//            'password' =>'root',
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        'ariel' => array(
            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
            'connectionString' => "mysql:host=127.0.0.1;dbname=ariel",
            'charset' => 'UTF8',
            'initSQLs' => array("SET session time_zone = '-3:00';","SET lc_time_names = 'es_AR'"),
            'username' => 'root',
            'password' => "lani0363",
//            'password' =>'root',
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        'dba' => array(
            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
            'connectionString' => "mysql:host=127.0.0.1;dbname=iae_audit",
            'charset' => 'UTF8',
            'initSQLs' => array("SET session time_zone = '-3:00';"),
            'username' => 'root',
            'password' => $passBase,
//            'password' =>'root',
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        "libs" => array("class" => "Libs"),
        'ck' => array(
            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=iae.dyndns.org;dbname=consignaciones',
            'connectionString' => 'mysql:host=127.0.0.1;dbname=criticker',
            'charset' => 'UTF8',
            //'initSQLs'=>array("SET session time_zone = '+0:00';"),
            'username' => 'root',
            'password' => "lani0363",
            'enableProfiling' => false,
            'enableParamLogging' => true,
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'defaultRoles' => array("Authenticated"),
            'assignmentTable' => 'authassignment',
            'itemTable' => 'authitem',
            'itemChildTable' => 'authitemchild',
            'rightsTable' => 'rights',
        ),
        'user' => array(
            'class' => 'RWebUser',
            'allowAutoLogin' => true,
        ),
        // uncomment the following to use a MySQL database
        /*
          'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=testdrive',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          ),
         */
					'errorHandler' => array(
							'errorAction' => 'site/error',
             'maxSourceLines' => 2000,
					),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace',
                    'categories' => 'vardump',
                    'showInFireBug' => true,
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(),
);
