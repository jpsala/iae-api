<?php

class MyPdf extends TCPDF {
	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
		
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
	}
	public function Header() {
		parent::Header();
	}
	public function Footer() {
		parent::Footer();
	}
}
