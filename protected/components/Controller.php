<?php

	/**
	 * Controller is the customized base controller class.
	 * All controller classes for this application should extend from this base class.
	 */
	class Controller extends CController {

		public function beforeAction($action) {
			Yii::app()->session->setTimeOut(Yii::app()->session->timeout);
			return parent::beforeAction($action);
		}

		public $pageTitle = "";
		public $comentarios = true;
		public $bootstrap = false;

		public function getActionParams() {
			return array_merge($_GET, $_POST);
		}

		/**
		 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
		 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
		 */
		public $layout = '//layouts/column1';

		/**
		 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
		 */
		public $menu = array();

		/**
		 * @var array the breadcrumbs of the current page. The value of this property will
		 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
		 * for more details on how to specify this property.
		 */
		public $breadcrumbs = array();

//  public function render($view, $data = null, $return = false) {
//    echo $this->getUniqueId();
//    if ($this->getUniqueId() !== "site" and !Yii::app()->user->isGuest and Yii::app()->user->model->cambiar_password) {
//      $siteC = new SiteController("site");
//      $siteC->render("/site/cambioPassword");
//    } else {
//      return parent::render($view, $data = null, $return = false);
//    }
//  }
	}
	