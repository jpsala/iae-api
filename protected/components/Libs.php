<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class Libs extends stdClass {

	private static $libs;
	private static $bootStrapTipo = "standard";

	public function init() {
		self::$libs = array();
	}

	public function add($lib) {
		if (is_array($lib)) {
			foreach ($lib as $l) {
				self::$libs[$l] = true;
			}
		} else {
			self::$libs[$lib] = true;
		}
	}

	public function bootStrap($tipo = "standard") {
		self::$bootStrapTipo = $tipo;
	}

	public function del($lib) {
		self::$libs[$lib] = false;
	}

	public function active($lib) {
		return isset(self::$libs[$lib]) and self::$libs[$lib];
	}

	public function includeBootStrap($tipo = null) {
		self::$bootStrapTipo = $tipo ? $tipo : self::$bootStrapTipo;
		?>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css" />
		<?php if (self::$bootStrapTipo == "standard"): ?>
			<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
			<script src="bootstrap/js/bootstrap.min.js"></script>
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.chico.min.css" />
			<script src="bootstrap/js/bootstrap.chico.min.js"></script>
		<?php endif; ?>
		<!--<link href="js/bootstrap-responsive.min.css" rel="stylesheet">-->
		<?php
	}

}
