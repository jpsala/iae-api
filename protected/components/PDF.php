<?php

require_once('tcpdf/config/lang/spa.php');
require_once('tcpdf/tcpdf.php');

class PDF extends TCPDF {

    public $marginX = 0, $marginY = 0;

    public function __set($name, $value) {
        if(strtoupper($name) =="Y"){
            $ant = $this->GetY();
            return $this->setY($value);
        }
        if(strtoupper($name) =="X"){
            $ant = $this->GetX();
            $this->setX($value);
        }
    }

    public function __get($name) {
        if(strtoupper($name) =="Y"){
            return $this->GetY();
        }
        if(strtoupper($name) =="X"){
            return $this->GetX();
        }
    }
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        $this->marginX = 0;
        $this->marginY = 0;
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

    public function Header() {
        $this->setJPEGQuality(90);
        $this->SetMargins($this->marginX, $this->marginY, 0, true);
        $this->SetY($this->marginY);
        $this->SetX($this->marginX);
        //$img = Yii::app()->createAbsoluteUrl("/images/iae25.jpg");
        //$this->Image($img, "", "", 15, '', "JPG", '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    public function Footer() {
        $this->SetY(0);
        $this->SetX(0);
    }

    /*
     * ( 	
     *  
      $  	w,
      $  	h,
      $  	txt,
      $  	border = 0,
      $  	align = 'J',
      $  	fill = false,
      $  	ln = 1,
      $  	x = '',
      $  	y = '',
      $  	reseth = true,
      $  	stretch = 0,
      $  	ishtml = false,
      $  	autopadding = true,
      $  	maxh = 0,
      $  	valign = 'T',
      $  	fitcell = false
      )
     */

    /**
     * This method allows printing text with line breaks.
     * They can be automatic (as soon as the text reaches the right border of the cell) or explicit (via the \n character). As many cells as necessary are output, one below the other.<br />
     * Text can be aligned, centered or justified. The cell block can be framed and the background painted.
     * @param $w (float) Width of cells. If 0, they extend up to the right margin of the page.
     * @param $h (float) Cell minimum height. The cell extends automatically if needed.
     * @param $txt (string) String to print
     * @param $border (mixed) Indicates if borders must be drawn around the cell. The value can be a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul> or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul> or an array of line styles for each border group - for example: array('LTRB' => array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)))
     * @param $align (string) Allows to center or align the text. Possible values are:<ul><li>L or empty string: left align</li><li>C: center</li><li>R: right align</li><li>J: justification (default value when $ishtml=false)</li></ul>
     * @param $fill (boolean) Indicates if the cell background must be painted (true) or transparent (false).
     * @param $ln (int) Indicates where the current position should go after the call. Possible values are:<ul><li>0: to the right</li><li>1: to the beginning of the next line [DEFAULT]</li><li>2: below</li></ul>
     * @param $x (float) x position in user units
     * @param $y (float) y position in user units
     * @param $reseth (boolean) if true reset the last cell height (default true).
     * @param $stretch (int) font stretch mode: <ul><li>0 = disabled</li><li>1 = horizontal scaling only if text is larger than cell width</li><li>2 = forced horizontal scaling to fit cell width</li><li>3 = character spacing only if text is larger than cell width</li><li>4 = forced character spacing to fit cell width</li></ul> General font stretching and scaling values will be preserved when possible.
     * @param $ishtml (boolean) INTERNAL USE ONLY -- set to true if $txt is HTML content (default = false). Never set this parameter to true, use instead writeHTMLCell() or writeHTML() methods.
     * @param $autopadding (boolean) if true, uses internal padding and automatically adjust it to account for line width.
     * @param $maxh (float) maximum height. It should be >= $h and less then remaining space to the bottom of the page, or 0 for disable this feature. This feature works only when $ishtml=false.
     * @param $valign (string) Vertical alignment of text (requires $maxh = $h > 0). Possible values are:<ul><li>T: TOP</li><li>M: middle</li><li>B: bottom</li></ul>. This feature works only when $ishtml=false and the cell must fit in a single page.
     * @param $fitcell (boolean) if true attempt to fit all the text within the cell by reducing the font size (do not work in HTML mode).
     * @return int Return the number of cells or 1 for html mode.
     * @public
     * @since 1.3
     * @see SetFont(), SetDrawColor(), SetFillColor(), SetTextColor(), SetLineWidth(), Cell(), Write(), SetAutoPageBreak()
     */
    public function textBox($textval = "", $width = null, $ln = null, $x = null, $y = null
    , $height = null
    , $border = 0, $align = "L", $fill = false, $reseth = false
    , $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T'
    , $fitcell = false) { //4791450 jardin
        if (!is_array($textval)) {
            return $this->textBoxInt($textval, $width, $ln, $x, $y
                            , $height
                            , $border, $align, $fill, $reseth
                            , $stretch, $ishtml, $autopadding, $maxh, $valign
                            , $fitcell);
        }
        $defaults = array(
                "w" => "",
                "h" => "",
                "txt" => "",
                "border" => "",
                "align" => "",
                "fill" => "",
                "ln" => "",
                "x" => $this->x,
                "y" => $this->y,
                "reseth" => true,
                "stretch" => "",
                "ishtml" => "",
                "autopadding" => "",
                "maxh" => "",
                "valign" => "",
                "fitcell" => "",
                "fontsize" => "",
                "fontstyle" => "",
        );
        if (isset($textval["fontsize"])) {
            $fs = $this->SetFont(PDF_FONT_NAME_MAIN, "", $textval["fontsize"]);
        }
        $fontStyle = $this->getFontStyle();
        if (isset($textval["fontstyle"])) {
            $this->SetFont(PDF_FONT_NAME_MAIN, $textval["fontstyle"]);
        }
        $args = array_merge($defaults, $textval);
        $this->MultiCell(
                $args["w"], $args["h"], $args["txt"], $args["border"], $args["align"], $args["fill"], $args["ln"], $args["x"], $args["y"], $args["reseth"], $args["stretch"], $args["ishtml"], $args["autopadding"], $args["maxh"], $args["valign"], $args["fitcell"]
        );
        if (isset($textval["fontstyle"])) {
            $this->SetFont(PDF_FONT_NAME_MAIN, $fontStyle);
        }
        if (isset($textval["fontsize"])) {
            $this->SetFont(PDF_FONT_NAME_MAIN, "", $fs);
        }

    }

    public function CreateTextBox($textval, $x = "", $y = "", $width = 10, $height = 0
    , $fontsize = 10, $fontstyle = '', $align = 'L', $valign = "T", $fitcell = false
    ) {
        $this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
        $this->MultiCell($width, $height, $textval, 0, $align, false, 1, $x, $y
                , true, 0, false, true, 0, $valign, $fitcell);
    }

    private function textBoxInt($textval = "", $width = null, $ln = null, $x = null, $y = null
    , $height = null
    , $border = 0, $align = "L", $fill = false, $reseth = false
    , $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T'
    , $fitcell = false) {
        if (!$width) {
            $width = $this->getStringWidth($textval) + 1;
            //$width = ($w > $width) ? $w : $width;
        }
        $height = ($height) ? $height : $this->getStringHeight($width, $textval);
        $this->MultiCell($width, $height, $textval
                , $border, $align, $fill, $ln, $x, $y, $reseth
                , $stretch, $ishtml, $autopadding, $maxh, $valign
                , $fitcell);
    }

    public function t($txt, $x = null, $y = null, $align = "L", $width = null) {
        $w = $width ? $width : $this->getStringWidth($txt, "", false, 0, false) + 2;
        $h = $this->getStringHeight($w, $txt);
        $this->textBox(array(
                "txt" => $txt,
                "y" => $y,
                "x" => $x,
                "h" => $h,
                "w" => $w,
                "align" => $align,
        ));
        return $h;
    }

}

?>
