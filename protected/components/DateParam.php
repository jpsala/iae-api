<?php

class DateParam extends Param {
  public function __construct($p, $nombre, $default = null) {
    parent::__construct($nombre, $p, $default);
    if(!$this->value){
      $this->value = date("d/m/Y",time());
    }
    $this->js .= "\$(\"#$this->id\").datepicker();";
  }

}

?>