<?php

class Params extends CComponent {

  public $items = array(), $defaultID, $html, $style, $js = "", $action, $params, $buttons;

  public function __construct($parametros) {
    $default = isset($parametros["default"]) ? $parametros["default"] : null;
    $this->action = isset($parametros["action"]) ? $parametros["action"] : null;
    $this->style = isset($parametros["style"]) ? $parametros["style"] : null;
    $this->buttons = isset($parametros["buttons"]) ? $parametros["buttons"] : array();
    foreach ($parametros["items"] as $nombre => $p) {
      $par = null;
      $parDefault = isset($p["default"]) ? $p["default"] : null;
      switch (mb_strtolower($p["tipo"])) {
        case "string":
          $par = new StringParam($p, $nombre, $parDefault);
          break;
        case "date":
          $par = new DateParam($p, $nombre, $parDefault);
          break;
      }
      if ($default and $default == $nombre) {
        $this->defaultID = $par->id;
      }
      $this->items[] = $par;
    }
    $this->js .= isset($parametros["js"]) ? $parametros["js"] : "";
  }

  public function html() {
    $cr = "\r\n";
    $this->html = $this->action ? "<form id=\"form-params\" method=\"POST\" action=\"$this->action\">" . $cr : "";
    $this->html .= "<div id='div-params'>" . $cr;
    foreach ($this->items as $p) {
      if ($p->js) {
        $this->js .= $p->js . $cr;
      }
      $this->html .= $p->html() . $cr;
    }
    if ($this->defaultID) {
      $this->js .= "\$(\"#$this->defaultID\").focus();" . $cr;
    }
    if ((count($this->buttons) > 0) or $this->action) {
      $this->html .= "<div id=\"param-buttons\">";
      if ($this->action) {
        $this->html .= "<input id=\"btn_params\" type='submit' value=\"Ok\" name=\"param_submit\"/>" . $cr;
      }
    }
    foreach ($this->buttons as $nombre => $button) {
      $action = $button["action"];
      $this->html .= "<input id=\"$nombre\" type='button' value=\"$nombre\"  name=\"$nombre\" onclick=\"$action\"/>" . $cr;
    }
    if ((count($this->buttons) > 0) or $this->action) {
      $this->html .= "</div>";
    }
    $this->html .= "</div>" . $cr;
    $this->html .= $this->action ? "</form>" . $cr : "";

    return $this->html;
  }

  public function htmlFull() {
    $cr = "\r\n";
    $style = "";
    $js = "";
    $html = $this->html();
    if ($this->style) {
      $style = "<style type=\"text/css\">" . $cr . $this->style . $cr . "</style>" . $cr;
    }
    if ($this->js) {
      $js = "<script type=\"text/javascript\">" . $cr . "$(function(){" . $cr . $this->js . $cr . "})" . $cr . "</script>" . $cr;
    }

    return $style . $js . $html;
  }

}

?>
