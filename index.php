<?php

if (
    !isset($_SERVER['HTTP_USER_AGENT']) or
    (isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'], 'Mozilla/4.0') > -1) or
    (isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'], 'Mozilla/3.0') > -1)
) {
    echo "<div style='font-size:80%;color:red'>" . $_SERVER['HTTP_USER_AGENT'];
    echo "<br/>";
    echo "Para evitar problemas de visualización pruebe con un navegador mas nuevo. Ej. IE9, chrome, firefox, etc";
    echo "</div>";
}
ini_set('max_execution_time', 300);
// change the following paths if necessary
if (gethostbyaddr("127.0.0.1") == "localhost") {
    //$yii = dirname(__FILE__) . '/../yii-1.1.13.e9e4a0/framework/yii.php';
    $yii = dirname(__FILE__) . '/framework/yii.php';
} else {
    $yii = dirname(__FILE__) . '/framework/yii.php';
}
$config = dirname(__FILE__) . '/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
//require_once('Var_Dump.php');
// Var_Dump::displayInit(array('display_mode' => 'HTML4_Table'));

define("EOL", "\r\n");
require('protected/helpers.php');
// var_dump(1);
// die;
require_once($yii);

//function fb($what){
//  echo Yii::log(CVarDumper::dumpAsString($what),'debug','vardump');
//}
setlocale(LC_ALL, "sp");
date_default_timezone_set('America/Argentina/Buenos_Aires');

Yii::createWebApplication($config)->run();