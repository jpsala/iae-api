#!/bin/sh
now=$(date -d "today" +"%Y%m%d-%H%M")
echo dump en IAE
ssh jpsala@iae.dyndns.org "sh /prg/iae/dump.sh"
rm iae-nuevo.dump 2>/dev/null

mv iae-nuevo.local.dump iae-nuevo.local.$now.dump 2>/dev/null
echo dump local
mysqldump -a -u root -plani0363 iae-nuevo > iae-nuevo.local.dump
echo bzip2 local
bzip2 iae-nuevo.local.dump
mv iae-nuevo.local.dump.bz2 iae-nuevo.local.dump.$now.bz2
echo drop
mysqladmin drop iae-nuevo -u root -plani0363 -f
echo create
mysqladmin create --default-character-set=utf8 iae-nuevo -u root --password=lani0363 -f

mv iae-nuevo.dump.bz2 iae-nuevo.dump.$now.bz2 2>/dev/null
echo scp
scp -C  jpsala@iae.dyndns.org:/prg/iae/iae-nuevo.dump.bz2  .
echo bunzip2
bunzip2 iae-nuevo.dump.bz2
echo import
pv iae-nuevo.dump |mysql iae-nuevo -u root --password=lani0363

