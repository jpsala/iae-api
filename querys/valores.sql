SELECT DATE_FORMAT(d.fecha_creacion, '%d/%m/%Y') AS fecha, CONCAT(dt.nombre, '-' , LPAD(d.numero, 8, '0')) AS comprobante, dvt.nombre AS valor, dv.importe * dt.signo_valor  AS importe,
  CASE WHEN !d.detalle then
    (SELECT GROUP_CONCAT(CONCAT(dt1.nombre, '-', d1.numero, '(', DATE_FORMAT(d1.fecha_creacion, '%d/%m/%y'), ') $', da.importe), ' ', '') 
        FROM doc_apl da
          INNER JOIN doc d1 ON da.doc_destino = d1.id
          INNER JOIN doc_tipo dt1 ON d1.doc_tipo_id = dt1.id
        WHERE da.doc_origen = d.id
        
    ) ELSE d.detalle end AS detalle
  FROM doc d
    inner JOIN doc_valor dv ON d.id = dv.doc_id
    inner JOIN doc_valor_tipo dvt ON dv.doc_valor_tipo_id = dvt.id
    inner JOIN doc_tipo dt ON d.doc_tipo_id = dt.id