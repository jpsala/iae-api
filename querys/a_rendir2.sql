SELECT dt.nombre, d.fecha_creacion, d.total, d.saldo, dr.cantidad, p.nombre AS producto
  FROM doc d 
    INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
    INNER JOIN doc_rendir dr ON d.id = dr.doc_rendicion_id
    INNER JOIN doc_det detcompra ON dr.doc_det_compra = detcompra.id
    INNER JOIN producto p ON detcompra.producto_id = p.id
WHERE dt.nombre = 'rendicion'