SELECT dt.nombre AS tipo, d.id, d.numero, d.socio_id, d.fecha_creacion, d.total, d.saldo, 
       (SELECT GROUP_CONCAT(CONCAT(dad.doc_destino, '_', dad.importe, '/')) FROM doc_apl dad WHERE dad.doc_origen = d.id)  as apl,
       (SELECT GROUP_CONCAT(CONCAT(p.nombre, ':', dd.cantidad, ':', dd.total, '/')) FROM doc_det dd inner JOIN producto p ON dd.producto_id = p.id WHERE dd.doc_id = d.id)  as det,
       (SELECT GROUP_CONCAT(CONCAT(dvt.nombre, '-',dv.importe, '/')) FROM doc_valor dv INNER JOIN doc_valor_tipo dvt ON dv.doc_valor_tipo_id = dvt.id WHERE dv.doc_id = d.id)  as valores
  FROM doc d
    left JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
ORDER BY d.id