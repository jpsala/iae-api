SELECT dt.nombre, date_format(d.fecha_creacion, '%d/%m/%Y') AS fecha, d.total, d.saldo, dr.cantidad, p.nombre AS producto, 
  CONCAT('#',doccompra.numero) AS compra, date_format(doccompra.fecha_creacion, '%d/%m/%Y') as fecha_compra, CONCAT('#',docventa.numero) AS venta, date_format(docventa.fecha_creacion, '%d/%m/%Y') as fecha_venta
  FROM doc d 
    INNER JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
    INNER JOIN doc_rendir dr ON d.id = dr.doc_rendicion_id
    INNER JOIN doc_det detcompra ON dr.doc_det_compra = detcompra.id
    INNER JOIN doc doccompra ON detcompra.doc_id = doccompra.id
    INNER JOIN doc_det detventa ON dr.doc_det_venta = detventa.id
    INNER JOIN doc docventa ON detventa.doc_id = docventa.id
    INNER JOIN producto p ON detcompra.producto_id = p.id
WHERE dt.nombre = 'rendicion'