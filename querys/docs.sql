SELECT d.id, s.nombre_completo, dt.nombre AS tipo, DATE_FORMAT(d.fecha_creacion, '%a %D') AS fecha, d.numero, d.total, d.saldo, p.nombre AS producto, dd.precio_compra, dd.cantidad, dd.descuento, dd.total, d.fecha_creacion
  FROM  doc d 
    left JOIN doc_det dd ON d.id = dd.doc_id
    left JOIN doc_tipo dt ON d.doc_tipo_id = dt.id
    left JOIN producto p ON dd.producto_id = p.id
    left JOIN socio s ON d.socio_id = s.id
  ORDER BY d.id desc