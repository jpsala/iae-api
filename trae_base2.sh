#!/bin/expect
spawn uname -n
expect {
  -re "jplinux|jpnote" {puts ok\n}
  eof  {send_user "No es una pc conocida\n"; exit 0}
  timeout   {send_user "Algo salió mal\n" ; exit 0 }
}
set now [exec date -d "today" +"%Y%m%d-%H%M"]
cd /prg/iae
send_user "Dump in IAE\n" 
spawn ssh jpsala@iae.dyndns.org "sh /prg/iae/dump.sh"
set timeout 300
expect {
  listo {send_user "Volví del IAE ok\n"}
  eof abort
  "Could not resolve" {send_user "problemas de conexión\n"; exit }
  timout {send_user "Algo salió mal\n" ; exit 0 }
}
set timeout 20
send_user "delete iae-nuevo.dump\n"
file delete -force iae-nuevo.dump
exec mv iae-nuevo.local.dump iae-nuevo.local.$now.dump 2>/dev/null
exec echo drop
exec mysqladmin drop iae-nuevo -u root -plani0363 -f
exec echo create
exec mysqladmin create --default-character-set=utf8 iae-nuevo -u root --password=lani0363 -f

exec mv iae-nuevo.dump.bz2 iae-nuevo.dump.$now.bz2 2>/dev/null
exec echo scp
exec scp -C  jpsala@iae.dyndns.org:/prg/iae/iae-nuevo.dump.bz2  .
exec echo bunzip2
exec bunzip2 iae-nuevo.dump.bz2
exec echo import
exec pv iae-nuevo.dump |mysql iae-nuevo -u root --password=lani0363

